import type { AxiosResponse } from 'axios';
import instance from '../configApi';
import {
  type TAuth,
  type TAuthReq,
  type TRestore,
  type TRefresh,
  type TNewPassword,
  type TLoginResponse,
  EAuthRoutes,
  TAuthCode,
  TSendCodeResponse,
  TAuthOrg,
} from './types';

export const register = (data: TAuth) => instance.post<TAuthReq>(EAuthRoutes.REGISTER, data);

export const verifyCode = (data: TAuthCode) =>
  instance.get<TAuthCode, AxiosResponse<TSendCodeResponse>>(EAuthRoutes.SEND_CODE + data.code);

export const registerOrg = (data: TAuthOrg) =>
  instance.post<TAuthOrg, AxiosResponse<TLoginResponse>>(EAuthRoutes.REGISTER_ORG, data);

export const login = (data: TAuth) =>
  instance.post<TAuthReq, AxiosResponse<TLoginResponse>>(EAuthRoutes.LOGIN, data);

export const restorePassword = (data: TRestore) =>
  instance.post<TRestore>(EAuthRoutes.RESTORE_PASSWORD, data);

export const newPassword = (data: TNewPassword) =>
  instance.post<TNewPassword, AxiosResponse<TLoginResponse>>(EAuthRoutes.NEW_PASSWORD, data);

export const refresh = (data: TRefresh) =>
  instance.post<TRefresh, AxiosResponse<TLoginResponse>>(EAuthRoutes.REFRESH, data);

export const userLogout = (data: TRefresh) => instance.post<TRefresh>(EAuthRoutes.LOGOUT, data);

export const deleteAccount = () => instance.post(EAuthRoutes.DELETE_ACCOUNT);
