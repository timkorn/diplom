export type TAuth = {
  email: string;
  password: string;
};

export type TAuthCode = {
  code: string;
};

export type TAuthReq = {
  email: string;
  password: string;
};

export type TAuthOrg = {
  address_legal: string;
  inn: string;
  title: string;
};

export type TRestore = {
  email: string;
};

export type TRefresh = {
  refresh: string;
};

export type TNewPassword = {
  new_password: string;
  restore_refresh: string;
};

export type TLoginResponse = {
  token: string;
  refresh: string;
};

export type TSendCodeResponse = {
  token: string;
};

export enum EAuthRoutes {
  REGISTER = 'users/legal/auth/sign_up/init',
  REGISTER_ORG = 'users/legal/auth/sign_up/complete',
  LOGIN = 'users/legal/auth/sign_in',
  SEND_CODE = 'users/legal/auth/sign_up/verify_code/',
  RESTORE_PASSWORD = 'users/legal/auth/restore_password',
  CONFIRM = 'users/legal/auth/confirm',
  REFRESH = 'users/legal/auth/refresh',
  NEW_PASSWORD = 'users/legal/auth/new_password',
  DELETE_ACCOUNT = 'users/legal/auth/delete_account',
  LOGOUT = 'users/legal/auth/log_out',
}

export const publicRoutes = Object.values(EAuthRoutes).filter(
  (x) =>
    x != EAuthRoutes.REGISTER_ORG && x != EAuthRoutes.LOGOUT && x != EAuthRoutes.DELETE_ACCOUNT,
) as string[];
