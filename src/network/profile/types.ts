export enum EProfileRoutes {
  GET_PROFILE = 'users/legal/profile/get_me',
  EDIT_PROFILE = 'users/legal/profile/profile',
}

export type TGetProfileResponse = {
  address_legal: string;
  email: string;
  inn: string;
  title: string;
};
