import instance from '../configApi';
import { EProfileRoutes, TGetProfileResponse } from './types';

export const getProfile = () => instance.get<TGetProfileResponse>(EProfileRoutes.GET_PROFILE);

export const editProfile = (data: TGetProfileResponse) =>
  instance.post<TGetProfileResponse>(EProfileRoutes.EDIT_PROFILE, data);
