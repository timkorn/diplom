export enum EFromRoutes {
  GET_FORM_PDF = 'forms/get_form_pdf_v2',
  GET_FORM_CONTENT = 'forms/get_form_content',
  GET_QR = 'qr/get_qr',
  GET_FORM_LIST = 'forms/get_list_for_company',
}

export type TGetFormData = {
  formId: string;
};

export type TGetFormContentResponse = {
  filename: string;
  first_party_name: string;
  form_fields: string[];
  form_title: string;
};

export type TGetFormListResponse = {
  forms: {
    created_at: number;
    first_party_name: string;
    id: number;
    template_id: number;
    template_title: string;
    title: string;
  }[];
};
