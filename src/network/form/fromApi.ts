import { AxiosResponse } from 'axios';
import instance from '../configApi';
import { EFromRoutes, TGetFormContentResponse, TGetFormData, TGetFormListResponse } from './types';

export const getFormPdfFromId = (data: TGetFormData) =>
  instance.get(`${EFromRoutes.GET_FORM_PDF}/${data.formId}`, {
    responseType: 'blob',
  });

export const getFormContentFromId = (data: TGetFormData) =>
  instance.get<TGetFormData, AxiosResponse<TGetFormContentResponse>>(
    `${EFromRoutes.GET_FORM_CONTENT}/${data.formId}`,
  );

export const getFormQr = (data: TGetFormData) =>
  instance.get(`${EFromRoutes.GET_QR}/${data.formId}`);

export const getFormList = () => instance.get<TGetFormListResponse>(`${EFromRoutes.GET_FORM_LIST}`);
