import axios, { AxiosError, AxiosHeaders, type InternalAxiosRequestConfig } from 'axios';
import { clientCookies } from '../utils/cookies';
import { refresh } from './auth/authApi';
import { EAuthRoutes, publicRoutes } from './auth/types';

const HOST_URL = 'http://77.232.143.172:8080/api/v1/';

const getRefresh = async () => {
  const refreshToken = clientCookies.getRefreshToken();
  if (refreshToken) {
    const response = await refresh({ refresh: refreshToken });
    clientCookies.setTokens(response.data);
    return 'Tokens were updated';
  }
  throw new Error();
};

const setAccessIntoHeader = (config: InternalAxiosRequestConfig<any>) => {
  if (config.url && !publicRoutes.includes(config.url) && config.headers) {
    const token = `Bearer ${clientCookies.getAccessToken()}`;
    (config.headers as AxiosHeaders).set('Authorization', token);
  }
  return config;
};

async function checkAuthError(error: AxiosError) {
  if (
    error.response?.status === 401 &&
    error.config &&
    error.config.url !== EAuthRoutes.REFRESH &&
    error.config.url !== EAuthRoutes.LOGIN
  ) {
    const newConfig = error.config;
    try {
      if (!checkAuthError.refresh) {
        checkAuthError.refresh = true;
        await getRefresh();
        checkAuthError.refresh = false;
      } else {
        await new Promise((resolve) => {
          const interval = setInterval(() => {
            if (!checkAuthError.refresh) {
              clearInterval(interval);
              resolve(1);
            }
          }, 100);
        });
      }
    } catch (err) {
      checkAuthError.refresh = false;
      clientCookies.deleteTokens();
      window.location.reload();

      throw error;
    }
    return instance(newConfig);
  }
  throw error;
}

checkAuthError.refresh = false as any;

const instance = axios.create({
  baseURL: HOST_URL,
});

instance.defaults.headers.common['Content-Type'] = 'application/json';

instance.interceptors.request.use(setAccessIntoHeader);
instance.interceptors.response.use((value) => value, checkAuthError);

export default instance;
