import instance from '../configApi';
import {
  EContractsRoutes,
  TContractListResponse,
  TGetContractData,
  TGetContractResponse,
} from './types';

export const getContractList = () =>
  instance.get<TContractListResponse>(`${EContractsRoutes.GET_CONTRACT_LIST}`);

export const getContractPdfFromId = (data: TGetContractData) =>
  instance.get(`${EContractsRoutes.GET_CONTRACT_PDF}/${data.contractId}`, {
    responseType: 'blob',
  });

export const getContract = (data: TGetContractData) =>
  instance.get<TGetContractResponse>(`${EContractsRoutes.GET_CONTRACT}/${data.contractId}`);
