export enum EContractsRoutes {
  GET_CONTRACT_LIST = 'contracts/get_list_for_company',
  GET_CONTRACT_PDF = 'contracts/get_contract_pdf',
  GET_CONTRACT = 'contracts/get_contract_content',
}

export type TContractListResponse = {
  contracts: {
    created_at: number;
    first_party_name: string;
    id: number;
    second_party_name: string;
    template_title: string;
    title: string;
  }[];
};

export type TGetContractData = {
  contractId: string;
};

export type TGetContractResponse = {
  contract_title: string;
  date: number;
  first_party_name: string;
  second_party_name: string;
};
