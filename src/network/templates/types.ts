export enum ETemplatesRoutes {
  GET_TEMPLATE_LIST = 'templates/get_list',
  GET_TEMPLATE_BY_ID = 'templates/get_template_pdf_v2',
  GET_TEMPLATE_FIELDS_BY_ID = 'templates/get_template_fields',
  FILL_TEMPLATE_FIELDS = 'templates/fill_template_fields_no_qr',
}

export type TGetTemplateListResponse = {
  templates: { id: Number; title: string }[];
};

export type TGetTemplate = {
  sampleId: string;
};

export type TGetTemplateFieldsResponse = {
  form_id: number;
  template_fields: string[];
};

export type TFillTemplateFields = {
  initiating_party_keys: {
    [key: string]: string;
  };
  template_id: number;
  form_name: string;
};

export type TFillTemplateFieldsResponse = {
  form_id: string;
};
