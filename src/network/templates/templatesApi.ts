import { AxiosResponse } from 'axios';
import instance from '../configApi';
import {
  ETemplatesRoutes,
  TFillTemplateFields,
  TFillTemplateFieldsResponse,
  TGetTemplate,
  TGetTemplateFieldsResponse,
  TGetTemplateListResponse,
} from './types';

export const getTemplateList = () =>
  instance.get<TGetTemplateListResponse>(ETemplatesRoutes.GET_TEMPLATE_LIST);

export const getTemplateById = (data: TGetTemplate) =>
  instance.get<TGetTemplate, AxiosResponse<any>>(
    `${ETemplatesRoutes.GET_TEMPLATE_BY_ID}/${data.sampleId}`,
    { responseType: 'blob' },
  );

export const getTemplateFieldsById = (data: TGetTemplate) =>
  instance.get<TGetTemplate, AxiosResponse<TGetTemplateFieldsResponse>>(
    `${ETemplatesRoutes.GET_TEMPLATE_FIELDS_BY_ID}/${data.sampleId}`,
  );

export const fillTemplateFields = (data: TFillTemplateFields) =>
  instance.post<TFillTemplateFields, AxiosResponse<TFillTemplateFieldsResponse>>(
    `${ETemplatesRoutes.FILL_TEMPLATE_FIELDS}`,
    data,
  );
