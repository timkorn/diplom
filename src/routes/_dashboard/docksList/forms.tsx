import { createFileRoute } from '@tanstack/react-router';
import { DocumentsList } from '../../../components/DocumentsList';

export const Route = createFileRoute('/_dashboard/docksList/forms')({
  component: () => <DocumentsList tab={'FORMS'} />,
});
