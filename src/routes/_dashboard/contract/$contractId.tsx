import { createFileRoute } from '@tanstack/react-router';
import { Contract } from '../../../components/Contract';

export const Route = createFileRoute('/_dashboard/contract/$contractId')({
  component: () => <Contract />,
});
