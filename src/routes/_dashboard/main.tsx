import { createFileRoute, redirect } from '@tanstack/react-router';

export const Route = createFileRoute('/_dashboard/main')({
  beforeLoad: () => {
    throw redirect({
      to: '/template/newTemplate',
    });
  },
  component: () => <></>,
});
