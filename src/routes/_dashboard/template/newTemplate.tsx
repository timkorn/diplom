import { createFileRoute } from '@tanstack/react-router';
import { NewTemplatePage } from '../../../components/TemplateList';

export const Route = createFileRoute('/_dashboard/template/newTemplate')({
  component: () => <NewTemplatePage />,
});
