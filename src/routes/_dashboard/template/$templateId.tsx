import { createFileRoute } from '@tanstack/react-router';
import { NewContractCreation } from '../../../components/NewTemplateCreation';

export const Route = createFileRoute('/_dashboard/template/$templateId')({
  component: () => <NewContractCreation />,
});
