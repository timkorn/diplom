import { createFileRoute } from '@tanstack/react-router';
import { ContractForm } from '../../../components/ContractForm';

export const Route = createFileRoute('/_dashboard/form/$formId')({
  component: () => <ContractForm />,
});
