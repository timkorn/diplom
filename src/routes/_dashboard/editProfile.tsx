import { createFileRoute } from '@tanstack/react-router';
import { EditProfile } from '../../components/EditProfile';

export const Route = createFileRoute('/_dashboard/editProfile')({
  component: () => <EditProfile />,
});
