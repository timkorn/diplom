import { createRootRoute, Outlet } from '@tanstack/react-router';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import type { AxiosError } from 'axios';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const retryFucntion = (count: number, err: unknown) => {
  const error = err as AxiosError;
  if (error.response && error.response.status) {
    return count < 2 && error.response.status === 401 && error.message === 'Refetch';
  }
  return false;
};

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: retryFucntion,
    },
    mutations: {
      retry: retryFucntion,
    },
  },
});

export const Route = createRootRoute({
  component: () => (
    <div className="h-screen w-screen bg-[#FBFBFF]">
      <QueryClientProvider client={queryClient}>
        <Outlet />
      </QueryClientProvider>
      <ToastContainer />
    </div>
  ),
});
