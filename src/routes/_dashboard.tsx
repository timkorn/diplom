import { createFileRoute, redirect } from '@tanstack/react-router';
import { clientCookies } from '../utils/cookies';
import { SidebarLayout } from '../components/SidebarLayout';

export const Route = createFileRoute('/_dashboard')({
  beforeLoad: async () => {
    const areTokensExist = clientCookies.checkTokens();

    if (!areTokensExist) {
      throw redirect({
        to: '/login',
      });
    }
  },
  component: () => <SidebarLayout />,
});
