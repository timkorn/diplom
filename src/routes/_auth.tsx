import { createFileRoute, Outlet, redirect } from '@tanstack/react-router';
import { clientCookies } from '../utils/cookies';

export const Route = createFileRoute('/_auth')({
  beforeLoad: async () => {
    const areTokensExist = clientCookies.checkTokens();

    if (areTokensExist) {
      throw redirect({
        to: '/',
      });
    }
  },
  component: () => <Outlet />,
});
