import { createFileRoute } from '@tanstack/react-router';
import { LoginOrganisation } from '../../components/RegisterOrg';

export const Route = createFileRoute('/_auth/registerOrg')({
  component: LoginOrganisation,
});
