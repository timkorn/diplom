import { createLazyFileRoute } from '@tanstack/react-router';
import { Register } from '../../components/Register';

export const Route = createLazyFileRoute('/_auth/register')({
  component: Register,
});
