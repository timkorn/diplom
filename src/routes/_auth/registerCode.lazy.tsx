import { createLazyFileRoute } from '@tanstack/react-router';
import { LoginCode } from '../../components/RegisterCode';

export const Route = createLazyFileRoute('/_auth/registerCode')({
  component: LoginCode,
});
