import { useMutation, useQuery } from '@tanstack/react-query';
import { useNavigate, useParams } from '@tanstack/react-router';
import { useMemo, useState } from 'react';
import {
  fillTemplateFields,
  getTemplateById,
  getTemplateFieldsById,
} from '../../network/templates/templatesApi';
import { TFillTemplateFields } from '../../network/templates/types';
import { PdfViewer } from '../PdfViewer';
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { checkAllFieldsToExist } from '../../utils/validation';

export const NewContractCreation = () => {
  const [pdfData, setPdfData] = useState<string | null>(null);

  const navigate = useNavigate();

  const sampleId = useParams({
    from: '/_dashboard/template/$templateId',
    select: (params) => params.templateId,
  });

  const templatePdfQuery = useQuery({
    queryKey: ['templatePdf', sampleId],
    queryFn: async () =>
      getTemplateById({ sampleId }).then(async (data) => {
        try {
          const pdfFile = new File([data.data], 'file.pdf', { type: 'application/pdf' });
          const pdfUrl = URL.createObjectURL(pdfFile);
          setPdfData(pdfUrl);
          return data;
        } catch (error) {
          alert(error);
          throw error;
        }
      }),
  });

  const getFieldsQuery = useQuery({
    queryKey: ['templateFields', sampleId],
    queryFn: async () => {
      const response = await getTemplateFieldsById({ sampleId });
      return response.data.template_fields;
    },
  });

  const fillFields = useMutation({
    mutationFn: (data: TFillTemplateFields) => {
      return fillTemplateFields(data);
    },
    onSuccess: (response) => {
      navigate({
        to: '/form/' + response.data.form_id,
      });
    },
  });

  const initialFormObject = useMemo(() => {
    if (!getFieldsQuery.data) {
      return null;
    }
    const obj = getFieldsQuery.data.reduce<{ [key: string]: string }>((obj, key) => {
      obj[key] = '';
      return obj;
    }, {});

    const resultObject = { ...obj, form_name: '' };

    return resultObject;
  }, [getFieldsQuery.data]);

  const areFieldsReady = !!initialFormObject && pdfData;

  return (
    <div className="flex max-h-screen w-full">
      <div className="flex max-h-screen grow flex-col overflow-auto pl-[70px] pt-[30px]">
        <div className="grow-0  text-xl font-semibold max-md:max-w-full">Создание документа</div>
        <div className="mt-[60px] w-[700px]">
          {!pdfData && <div className="skeleton h-[600px] rounded-[30px]" />}
          {templatePdfQuery.isError && <div>Шаблон не найден</div>}
          {pdfData && <PdfViewer url={pdfData} />}
        </div>
      </div>
      <div className="flex h-screen w-[750px] flex-col  justify-self-end bg-white">
        {!areFieldsReady && (
          <div className="flex h-full w-full flex-col gap-[16px] px-[12px] pt-4">
            <div className="skeleton h-[40px] w-full rounded-[12px]" />
            <div className="skeleton h-[40px] w-full rounded-[12px]" />
            <div className="skeleton h-[40px] w-full rounded-[12px]" />
            <div className="skeleton h-[40px] w-full rounded-[12px]" />
          </div>
        )}
        {areFieldsReady && (
          <Formik
            initialValues={initialFormObject}
            onSubmit={(values) => {
              fillFields.mutate({
                template_id: parseInt(sampleId),
                initiating_party_keys: Object.fromEntries(
                  Object.entries(values).filter((item) => item[0] !== 'form_name'),
                ),
                form_name: values.form_name,
              });
            }}
            validationSchema={toFormikValidationSchema(checkAllFieldsToExist(initialFormObject))}
          >
            {() => (
              <Form className="flex h-full w-full flex-col pt-4">
                <div className="flex h-[50px] items-center justify-between  gap-[24px] border-b-[1px] px-[14px] pb-[24px]">
                  <div className="grow-0 max-md:max-w-full">Название документа:</div>
                  <div className="grow">
                    <CustomInput name={'form_name'} type="text" placeholder={'Название'} />
                  </div>
                </div>
                <div className="flex grow flex-col gap-[12px] overflow-auto pt-[10px]">
                  {Object.keys(initialFormObject)
                    .filter((item) => item !== 'form_name')
                    .map((key) => (
                      <div>
                        <div className="px-[14px] pb-[8px]">
                          <CustomInput name={key} type="text" label={key.split('_').join(' ')} />
                        </div>
                        <div className="mt-[12px] h-[1px] w-full bg-gray-200" />
                      </div>
                    ))}
                </div>

                <button
                  className={`btn btn-primary h-[80px] w-full !rounded-none text-[16px] ${fillFields.isPending && 'btn-loading'}`}
                  type="submit"
                >
                  Создать
                </button>
              </Form>
            )}
          </Formik>
        )}
      </div>
    </div>
  );
};
