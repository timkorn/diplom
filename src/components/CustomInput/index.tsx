import { useField } from 'formik';

/* interface CustomInputProps extends FieldInputProps<string> {
  name: string;
  type: string;
  placeholder?: string; 
} */

const CustomInput = (props: any) => {
  const [field, meta] = useField(props);
  const isError = meta.error && meta.touched;

  const inputErrorStyle = isError ? 'input-error' : '';
  const errorMargin = props.label ? 'top-[68px]' : 'top-10';

  return (
    <div className="form-field relative">
      {props.label && (
        <label className="form-label">
          <span>{props.label}</span>
        </label>
      )}
      <input className={`input max-w-full ${inputErrorStyle}`} {...field} {...props} />
      {isError && (
        <label className={'form-label absolute ' + errorMargin}>
          <span className="form-label-alt text-error">{meta.error}</span>
        </label>
      )}
    </div>
  );
};

export default CustomInput;
