import { Formik, Form } from 'formik';
import CustomInput from '../CustomInput';
import { useNavigate } from '@tanstack/react-router';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-toastify';
import { TAuthCode } from '../../network/auth/types';
import { verifyCode } from '../../network/auth/authApi';
import { AxiosError } from 'axios';
import { clientCookies } from '../../utils/cookies';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { sendCodeSchema } from '../../utils/validation';
import Button from '../Button';

export const LoginCode = () => {
  const navigate = useNavigate();

  const mutation = useMutation({
    mutationFn: (formData: TAuthCode) => {
      return verifyCode(formData);
    },
    onError: (error: AxiosError) => {
      if (error.request.status == 400) {
        toast.error('Неверный код');
      } else {
        toast.error('Ошибка на сервере');
      }
    },
    onSuccess: (data) => {
      clientCookies.setAccessToken(data.data.token);
      navigate({
        to: '/registerOrg',
      });
    },
  });
  return (
    <div className="w-fill flex h-full items-center">
      <div className="mx-auto flex w-full max-w-sm flex-col  gap-12">
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-semibold">Введите код из email</h1>
        </div>

        <Formik
          initialValues={{
            code: '',
          }}
          onSubmit={(values) => mutation.mutate({ code: values.code })}
          validationSchema={toFormikValidationSchema(sendCodeSchema)}
        >
          {() => (
            <Form>
              <div className="form-group">
                <CustomInput name="code" type="text" placeholder="Код" />

                <div className="form-field pt-5">
                  <div className="form-control justify-between">
                    <Button type="submit" isLoading={mutation.isPending}>
                      Далее
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
