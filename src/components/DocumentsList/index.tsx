import { useQuery } from '@tanstack/react-query';
import { Link } from '@tanstack/react-router';
import { getContractList } from '../../network/contracts/contractsApi';
import { getFormList } from '../../network/form/fromApi';
import { fromSecondsToDMY } from '../../utils/date';

type Props = {
  tab: 'FORMS' | 'CONTRACTS';
};

const ListElement = (props: {
  mainTitle: string;
  secondaryTitle: string;
  date: number;
  link: string;
  secondPartyName?: string;
}) => {
  const resultDate = fromSecondsToDMY(props.date);

  return (
    <Link to={props.link}>
      <div className="flex max-w-[754px] flex-col rounded-xl border-[1px] border-gray-300 bg-white px-5 py-3 shadow-sm hover:cursor-pointer max-md:px-5">
        <div className="text-neutral-800 flex w-full justify-between gap-3 self-start ">
          <div className="flex gap-[12px]">
            <svg
              width="16"
              height="21"
              viewBox="0 0 16 21"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1 17.3793V3.44824C1 2.34367 1.89543 1.44824 3 1.44824H9.23722C9.77263 1.44824 10.2857 1.66292 10.6616 2.04422L14.4243 5.86142C14.7932 6.23563 15 6.73998 15 7.26544V17.3793C15 18.4838 14.1046 19.3793 13 19.3793H3C1.89543 19.3793 1 18.4838 1 17.3793Z"
                stroke="#687590"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>

            <div className="my-auto flex-auto text-base font-medium">{props.mainTitle}</div>
          </div>
          <div className="text-base text-slate-500">{props.secondPartyName}</div>
        </div>
        <div className="mt-1.5 flex gap-5 text-slate-500 max-md:max-w-full max-md:flex-wrap">
          <div className="flex-auto text-base font-medium">{props.secondaryTitle}</div>
          <div className="text-base">{resultDate}</div>
        </div>
      </div>
    </Link>
  );
};

const getTabStyle = (isActive: Boolean) => {
  const commonStyle = 'tab px-6';
  const activeStyle = 'tab-underline tab-active';

  if (isActive) {
    return commonStyle + ' ' + activeStyle;
  }

  return commonStyle;
};

export const DocumentsList = ({ tab }: Props) => {
  const formListQuery = useQuery({
    queryKey: ['formList'],
    queryFn: getFormList,
  });

  const contractsListQuery = useQuery({
    queryKey: ['contractsList'],
    queryFn: getContractList,
  });

  const isLoading =
    (tab === 'FORMS' && formListQuery.isLoading) ||
    (tab === 'CONTRACTS' && contractsListQuery.isLoading);

  return (
    <div className="flex max-h-screen w-full">
      <div className="flex max-h-screen  w-full flex-col overflow-auto pl-[70px] pt-[30px]">
        <div className="grow-0  text-xl font-semibold">Мои договоры</div>
        <div className="tabs mt-[60px]">
          <Link to="/docksList/forms">
            <div className={getTabStyle(tab === 'FORMS')}>Формы</div>
          </Link>
          <Link to="/docksList/contracts">
            <div className={getTabStyle(tab === 'CONTRACTS')}>Договоры</div>
          </Link>
        </div>
        <div className="flex w-full grow flex-col gap-[12px] overflow-auto pb-[12px] pt-[30px]">
          {isLoading && (
            <>
              <div className="skeleton h-[70px] max-w-[754px] rounded-xl" />
              <div className="skeleton h-[70px] max-w-[754px] rounded-xl" />
              <div className="skeleton h-[70px] max-w-[754px] rounded-xl" />
              <div className="skeleton h-[70px]  max-w-[754px] rounded-xl" />
            </>
          )}
          {tab === 'FORMS' &&
            formListQuery.isSuccess &&
            ((formListQuery.data.data.forms.length === 0 && (
              <div className="ml-[300px] mt-[40px]">Список пуст</div>
            )) ||
              formListQuery.data.data.forms.map((item) => (
                <ListElement
                  date={item.created_at}
                  mainTitle={item.title}
                  secondaryTitle={item.template_title}
                  link={`/form/${item.id}`}
                />
              )))}
          {tab === 'CONTRACTS' &&
            contractsListQuery.isSuccess &&
            ((contractsListQuery.data.data.contracts.length === 0 && (
              <div className="ml-[300px] mt-[40px]">Список пуст</div>
            )) ||
              contractsListQuery.data.data.contracts.map((item) => (
                <ListElement
                  date={item.created_at}
                  mainTitle={item.title}
                  secondaryTitle={item.template_title}
                  link={`/contract/${item.id}`}
                  secondPartyName={item.second_party_name}
                />
              )))}
        </div>
      </div>
    </div>
  );
};
