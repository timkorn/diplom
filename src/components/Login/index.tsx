import { Formik, Form } from 'formik';
import CustomInput from '../CustomInput';
import { Link, useNavigate } from '@tanstack/react-router';
import { useMutation } from '@tanstack/react-query';
import { TAuth } from 'src/network/auth/types';
import { AxiosError } from 'axios';
import { toast } from 'react-toastify';
import { login } from '../../network/auth/authApi';
import { clientCookies } from '../../utils/cookies';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { loginSchema } from '../../utils/validation';
import Button from '../Button';

export const Login = () => {
  const navigate = useNavigate();

  const mutation = useMutation({
    mutationFn: (formData: TAuth) => {
      return login(formData);
    },
    onError: (error: AxiosError) => {
      if (error.request.status == 401 || error.request.status == 400) {
        toast.error('Неправильные логин или пароль');
      } else {
        toast.error('Ошибка на сервере');
      }
    },
    onSuccess: ({ data }) => {
      console.log('Success');
      clientCookies.setTokens(data);
      navigate({
        to: '/main',
      });
    },
  });

  return (
    <div className="w-fill flex h-full items-center">
      <div className="mx-auto mb-[200px] flex w-full max-w-sm flex-col gap-12">
        <div className="self-center">
          <img src="src/assets/logo_porykam.jpg" width="150px" height="150px" />
        </div>
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-semibold">Вход</h1>
        </div>

        <Formik
          initialValues={{
            email: '',
            password: '',
          }}
          onSubmit={(values) => mutation.mutate(values)}
          validationSchema={toFormikValidationSchema(loginSchema)}
        >
          {() => (
            <Form>
              <div className="form-group gap-5">
                <CustomInput name="email" type="text" placeholder="Почта" />
                <CustomInput name="password" type="password" placeholder="Пароль" />

                <div className="form-field pt-5">
                  <div className="form-control justify-between">
                    <Button type="submit" isLoading={mutation.isPending}>
                      Войти
                    </Button>
                  </div>
                </div>

                <div className="form-field">
                  <div className="form-control justify-center">
                    <Link className="link link-primary link-underline-hover text-sm" to="/register">
                      Нет аккаунта? Зарегистрироваться.
                    </Link>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
