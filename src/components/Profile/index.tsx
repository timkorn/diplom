import { useMutation, useQuery } from '@tanstack/react-query';
import { getProfile } from '../../network/profile/profileApi';
import { deleteAccount, userLogout } from '../../network/auth/authApi';
import { clientCookies } from '../../utils/cookies';
import { useNavigate } from '@tanstack/react-router';

interface ProfileElementProps {
  name: String;
  value: String | undefined;
}

const ProfileElement = ({ name, value }: ProfileElementProps) => {
  return (
    <div className="mw-[400px] min-w-[400px]">
      <div className="text-neutral-800 text-base font-semibold">{name} </div>

      {value ? (
        <div className="mt-3 font-medium text-slate-500 max-md:max-w-full">{value}</div>
      ) : (
        <div className="skeleton mt-3 h-5 rounded-lg" />
      )}
    </div>
  );
};

export const Profile = () => {
  const navigate = useNavigate();

  const { data } = useQuery({
    queryKey: ['profile'],
    queryFn: getProfile,
  });

  const logOut = useMutation({
    mutationFn: (refresh: string) => {
      return userLogout({ refresh });
    },
    onSuccess: () => {
      clientCookies.deleteTokens();
      navigate({ to: '/login' });
    },
  });

  const deleteUserAccount = useMutation({
    mutationFn: () => {
      return deleteAccount();
    },
    onSuccess: () => {
      clientCookies.deleteTokens();
      navigate({ to: '/login' });
    },
  });

  const goToEditProfile = () => {
    navigate({ to: '/editProfile' });
  };

  const logOutOnClick = () => {
    const refresh = clientCookies.getRefreshToken();
    logOut.mutate(refresh);
  };

  const deleteAccountClick = () => {
    deleteUserAccount.mutate();
  };

  const profile = data?.data;

  return (
    <div className="pl-[200px] pt-[30px]">
      <div className="max-w-[900px]">
        <div className="flex w-full justify-between">
          <div className="text-xl font-semibold max-md:max-w-full">Профиль</div>
          <div className="flex gap-[12px]">
            <button className="btn btn-outline-primary" onClick={goToEditProfile}>
              Редактировать профиль
            </button>

            <label className="btn btn-error" htmlFor="modal-1">
              Удалить аккаунт
            </label>
            <input className="modal-state" id="modal-1" type="checkbox" />
            <div className="modal">
              <label className="modal-overlay" htmlFor="modal-1"></label>
              <div className="modal-content flex h-[200px] w-[400px] flex-col justify-between">
                <label
                  htmlFor="modal-1"
                  className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                >
                  ✕
                </label>
                <h2 className="text-xl">Удаление аккаунта</h2>
                <span>Уверены, что хотите удалить аккаунт?</span>
                <div className="flex gap-3">
                  <label
                    className="btn btn-error btn-block"
                    htmlFor="modal-1"
                    onClick={deleteAccountClick}
                  >
                    Удалить
                  </label>
                  <label className="btn btn-block" htmlFor="modal-1">
                    Отмена
                  </label>
                </div>
              </div>
            </div>

            <label className="btn btn-outline-error" htmlFor="modal-2">
              Выйти
            </label>
            <input className="modal-state" id="modal-2" type="checkbox" />
            <div className="modal">
              <label className="modal-overlay" htmlFor="modal-2"></label>
              <div className="modal-content flex h-[200px] w-[400px] flex-col justify-between">
                <label
                  htmlFor="modal-2"
                  className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                >
                  ✕
                </label>
                <h2 className="text-xl">Выход</h2>
                <span>Уверены, что хотите выйти?</span>
                <div className="flex gap-3">
                  <label
                    className="btn btn-error btn-block"
                    htmlFor="modal-2"
                    onClick={logOutOnClick}
                  >
                    Выход
                  </label>
                  <label className="btn btn-block" htmlFor="modal-2">
                    Отмена
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-[60px] flex flex-row justify-between gap-6">
          <div className="flex flex-col gap-16">
            <ProfileElement name="ИНН" value={profile?.inn} />
            <ProfileElement name="Наименование" value={profile?.title} />
          </div>
          <div className="flex flex-col gap-16">
            <ProfileElement name="Адрес" value={profile?.address_legal} />
            <ProfileElement name="Почта" value={profile?.email} />
          </div>
        </div>
      </div>
    </div>
  );
};
