type Props = {
  isLoading?: Boolean;
  children: React.ReactNode;
  className?: string;
} & React.ButtonHTMLAttributes<HTMLButtonElement>;

const Button = ({ isLoading, children, ...otherProps }: Props) => {
  const loadingStyle = isLoading ? 'btn-loading' : '';

  return (
    <button type="submit" className={'btn btn-primary w-full' + ' ' + loadingStyle} {...otherProps}>
      {children}
    </button>
  );
};

export default Button;
