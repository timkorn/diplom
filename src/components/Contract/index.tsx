import { useQuery } from '@tanstack/react-query';
import { useParams } from '@tanstack/react-router';
import { useMemo, useState } from 'react';
import { PdfViewer } from '../PdfViewer';
import Button from '../Button';
import { getContract, getContractPdfFromId } from '../../network/contracts/contractsApi';
import { fromSecondsToDMY } from '../../utils/date';

export const Contract = () => {
  const [pdfData, setPdfData] = useState<string | null>(null);

  const contractId = useParams({
    from: '/_dashboard/contract/$contractId',
    select: (params) => params.contractId,
  });

  const pdfQuery = useQuery({
    queryKey: ['contractPdf', contractId],
    queryFn: async () =>
      getContractPdfFromId({ contractId }).then(async (data) => {
        try {
          const pdfFile = new File([data.data], 'file.pdf', { type: 'application/pdf' });
          const pdfUrl = URL.createObjectURL(pdfFile);
          setPdfData(pdfUrl);
          return data;
        } catch (error) {
          alert(error);
          throw error;
        }
      }),
  });

  const contractQuery = useQuery({
    queryKey: ['getContract', contractId],
    queryFn: () => getContract({ contractId }).then((response) => response.data),
  });

  const DMYdate = useMemo(() => {
    if (contractQuery.data) return fromSecondsToDMY(contractQuery.data.date);
  }, [contractQuery.data]);

  return (
    <div className="flex max-h-screen w-full">
      <div className="flex max-h-screen  flex-col overflow-auto pl-[70px] pt-[30px]">
        <div className="grow-0  text-xl font-semibold">Контракт</div>
        <div className="mt-[60px] flex w-[600px] flex-col">
          {!pdfData && <div className="skeleton h-[600px] rounded-[30px]" />}
          {pdfQuery.isError && <div>Форма не найдена</div>}
          {pdfData && <PdfViewer url={pdfData} />}
          {pdfData && (
            <a
              href={pdfData}
              download={'form.pdf'}
              className="absolute bottom-[80px] place-self-center"
            >
              <Button className="btn btn-solid-primary">Скачать PDF</Button>
            </a>
          )}
        </div>
      </div>
      <div className="ml-[100px] mt-[120px] flex w-[200px] flex-col gap-[24px]">
        {contractQuery.isSuccess && (
          <>
            <div>
              <div className="text-lg font-medium">Название</div>
              <div className="mt-[16px] text-slate-500">{contractQuery.data.contract_title}</div>
            </div>
            <div>
              <div className="text-lg font-medium">Подписант</div>
              <div className="mt-[16px] text-slate-500">{contractQuery.data.second_party_name}</div>
            </div>
            <div>
              <div className="text-lg font-medium">Дата созания</div>
              <div className="mt-[16px] text-slate-500">{DMYdate}</div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};
