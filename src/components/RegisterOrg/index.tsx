import { Formik, Form } from 'formik';
import CustomInput from '../CustomInput';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { registerOrgSchema } from '../../utils/validation';
import { useMutation } from '@tanstack/react-query';
import { registerOrg } from '../../network/auth/authApi';
import { AxiosError } from 'axios';
import { toast } from 'react-toastify';
import { TAuthOrg } from '../../network/auth/types';
import { useNavigate } from '@tanstack/react-router';
import { clientCookies } from '../../utils/cookies';
import Button from '../Button';

export const LoginOrganisation = () => {
  const navigate = useNavigate();

  const mutation = useMutation({
    mutationFn: (formData: TAuthOrg) => {
      return registerOrg(formData);
    },
    onError: (error: AxiosError) => {
      if (error.request.status == 400) {
        toast.error(error.message);
      } else {
        toast.error('Ошибка на сервере');
      }
    },
    onSuccess: ({ data }) => {
      console.log('Success');
      clientCookies.setTokens(data);
      navigate({
        to: '/main',
      });
    },
  });

  return (
    <div className="w-fill flex h-full items-center">
      <div className="mx-auto flex w-full max-w-sm flex-col  gap-12">
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-semibold">Данные организации</h1>
        </div>

        <Formik
          initialValues={{
            inn: '',
            title: '',
            address_legal: '',
          }}
          onSubmit={(data) => mutation.mutate(data)}
          validationSchema={toFormikValidationSchema(registerOrgSchema)}
        >
          {() => (
            <Form>
              <div className="form-group gap-5">
                <CustomInput name="inn" type="text" placeholder="ИНН" />
                <CustomInput name="title" type="text" placeholder="Название организации" />
                <CustomInput name="address_legal" type="text" placeholder="Адрес" />

                <div className="form-field pt-5">
                  <div className="form-control justify-between">
                    <Button type="submit" isLoading={mutation.isPending}>
                      Далее
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
