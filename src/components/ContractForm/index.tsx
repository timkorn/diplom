import { useQuery } from '@tanstack/react-query';
import { useParams } from '@tanstack/react-router';
import { useEffect, useState } from 'react';
import { PdfViewer } from '../PdfViewer';
import { getFormContentFromId, getFormPdfFromId, getFormQr } from '../../network/form/fromApi';
import Button from '../Button';

export const ContractForm = () => {
  const [pdfData, setPdfData] = useState<string | null>(null);
  const [qrElement, setQrElement] = useState<string | null>(null);
  const [qrUrl, setQrUrl] = useState<string | null>(null);

  const formId = useParams({
    from: '/_dashboard/form/$formId',
    select: (params) => params.formId,
  });

  const qrQuery = useQuery({
    queryKey: ['formQr', formId],
    queryFn: () => getFormQr({ formId }),
  });

  const formDataQuery = useQuery({
    queryKey: ['formData', formId],
    queryFn: () => getFormContentFromId({ formId }).then((response) => response.data),
  });

  const pdfQuery = useQuery({
    queryKey: ['formPdf', formId],
    queryFn: async () =>
      getFormPdfFromId({ formId }).then(async (data) => {
        try {
          const pdfFile = new File([data.data], 'file.pdf', { type: 'application/pdf' });
          const pdfUrl = URL.createObjectURL(pdfFile);
          setPdfData(pdfUrl);
          return data;
        } catch (error) {
          alert(error);
          throw error;
        }
      }),
  });

  useEffect(() => {
    const qrData = qrQuery.data?.data;
    if (qrData) {
      const svgElement = document.createElement('svg');
      svgElement.innerHTML = qrData;

      const viewBox = '0 0 100 100';
      const width = '200px';
      const height = '200px';

      svgElement.setAttribute('viewBox', viewBox);
      svgElement.setAttribute('height', height);
      svgElement.setAttribute('width', width);

      const qrFile = new File([svgElement.outerHTML], 'qr.svg', { type: 'image/svg+xml' });
      const qrUrl = URL.createObjectURL(qrFile);
      setQrUrl(qrUrl);

      setQrElement(svgElement.outerHTML);
    }
    () => {
      if (qrUrl) URL.revokeObjectURL(qrUrl);
    };
  }, [qrQuery.data?.data]);

  return (
    <div className="flex max-h-screen w-full">
      <div className="flex max-h-screen  flex-col overflow-auto pl-[70px] pt-[30px]">
        <div className="flex gap-[14px]">
          <div className="grow-0  text-xl font-semibold">Форма</div>
          {formDataQuery.isSuccess && (
            <div className="grow-0  text-xl font-semibold text-[#3185FC]">
              {formDataQuery.data.form_title}
            </div>
          )}
        </div>
        <div className="mt-[60px] flex w-[600px] flex-col">
          {!pdfData && <div className="skeleton h-[600px] rounded-[30px]" />}
          {pdfQuery.isError && <div>Форма не найдена</div>}
          {pdfData && <PdfViewer url={pdfData} />}
          {pdfData && formDataQuery.isSuccess && (
            <a
              href={pdfData}
              download={`${formDataQuery.data.form_title}.pdf`}
              className="absolute bottom-[80px] place-self-center"
            >
              <Button className="btn btn-solid-primary">Скачать PDF</Button>
            </a>
          )}
        </div>
      </div>
      <div className="ml-[80px] mt-[118px] flex flex-col items-center gap-[16px]">
        {qrElement && qrUrl && formDataQuery.isSuccess && (
          <>
            <div dangerouslySetInnerHTML={{ __html: qrElement }} />
            <a href={qrUrl} download={`${formDataQuery.data.form_title}.svg`}>
              <Button className="btn btn-outline-primary">Скачать QR</Button>
            </a>
            <div>ID: {formId}</div>
          </>
        )}
      </div>
    </div>
  );
};
