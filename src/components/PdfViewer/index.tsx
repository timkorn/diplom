// Import the main component
import { Viewer } from '@react-pdf-viewer/core';

import '@react-pdf-viewer/core/lib/styles/index.css';

export const PdfViewer = ({ url }: { url: string }) => (
  <div className="w-full overflow-hidden">
    <Viewer fileUrl={url} />
  </div>
);
