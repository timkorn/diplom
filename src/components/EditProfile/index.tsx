import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { editProfile, getProfile } from '../../network/profile/profileApi';

import { useNavigate } from '@tanstack/react-router';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { checkAllFieldsToExist } from '../../utils/validation';
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import { TGetProfileResponse } from 'src/network/profile/types';
import Button from '../Button';

interface ProfileElementProps {
  name: String;
  value?: String;
}

const ProfileElement = ({ name, value }: ProfileElementProps) => {
  return (
    <div className="mw-[400px] min-w-[400px]">
      <div className="text-neutral-800 text-base font-semibold">{name} </div>

      {value ? (
        <div className="mt-3 font-medium text-slate-500 max-md:max-w-full">{value}</div>
      ) : (
        <div className="skeleton mt-3 h-5 rounded-lg" />
      )}
    </div>
  );
};

export const EditProfile = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { data } = useQuery({
    queryKey: ['profile'],
    queryFn: getProfile,
  });

  const editMutation = useMutation({
    mutationFn: (formData: TGetProfileResponse) => {
      return editProfile(formData);
    },
    onError: () => {
      navigate({
        to: '/profile',
      });
    },
    onSuccess: () => {
      queryClient.removeQueries({ queryKey: ['profile'], exact: true });
      navigate({
        to: '/profile',
      });
    },
  });

  const profile = data?.data;

  const cancelEdit = () => {
    navigate({ to: '/profile' });
  };

  return (
    <div className="pl-[200px] pt-[30px]">
      <div className="max-w-[900px]">
        {profile ? (
          <>
            <Formik
              initialValues={profile}
              onSubmit={(values) => {
                editMutation.mutate(values);
              }}
              validationSchema={toFormikValidationSchema(checkAllFieldsToExist(profile))}
            >
              {() => (
                <Form>
                  <div className="flex w-full justify-between">
                    <div className="text-xl font-semibold max-md:max-w-full">
                      Редактировать профиль
                    </div>
                    <div className="flex gap-[12px]">
                      <button className="btn btn-outline-primary" onClick={cancelEdit}>
                        Отмена
                      </button>
                      <Button isLoading={editMutation.isPending}>Сохранить</Button>
                    </div>
                  </div>
                  <div className="mt-[60px] flex flex-row justify-between gap-6">
                    <div className="flex flex-col gap-16">
                      <CustomInput name={'inn'} type="text" label={'ИНН'} />
                      <CustomInput name={'title'} type="text" label={'Наименование'} />
                    </div>
                    <div className="flex flex-col gap-16">
                      <CustomInput name={'address_legal'} type="text" label={'Адрес'} />
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          </>
        ) : (
          <>
            <div className="text-xl font-semibold max-md:max-w-full">Редактировать профиль</div>
            <div className="mt-[60px] flex flex-row justify-between gap-6">
              <div className="flex flex-col gap-16">
                <ProfileElement name="ИНН" />
                <ProfileElement name="Наименование" />
              </div>
              <div className="flex flex-col gap-16">
                <ProfileElement name="Адрес" />
                <ProfileElement name="Почта" />
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};
