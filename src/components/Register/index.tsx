import { Formik, Form } from 'formik';
import CustomInput from '../CustomInput';
import { Link, useNavigate } from '@tanstack/react-router';
import { registrSchema } from '../../utils/validation';
import { toFormikValidationSchema } from 'zod-formik-adapter';
import { AxiosError } from 'axios';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-toastify';
import { TAuth } from 'src/network/auth/types';
import { register } from '../../network/auth/authApi';
import Button from '../Button';

export const Register = () => {
  const navigate = useNavigate();

  const mutation = useMutation({
    mutationFn: (formData: TAuth) => {
      return register(formData);
    },
    onError: (error: AxiosError) => {
      if (error.request.status == 403) {
        toast.error('Такой email уже зарегестрирован');
      } else {
        toast.error('Ошибка на сервере');
      }
    },
    onSuccess: () => {
      navigate({
        to: '/registerCode',
      });
    },
  });

  return (
    <div className="w-fill flex h-full items-center">
      <div className="mx-auto mb-[200px] flex w-full max-w-sm flex-col gap-12">
        <div className="self-center">
          <img src="src/assets/logo_porykam.jpg" width="150px" height="150px" />
        </div>
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-semibold">Регистрация</h1>
        </div>

        <Formik
          initialValues={{
            email: '',
            password: '',
            repeatPassword: '',
          }}
          onSubmit={(values) => mutation.mutate({ email: values.email, password: values.password })}
          validationSchema={toFormikValidationSchema(registrSchema)}
        >
          {() => (
            <Form>
              <div className="form-group gap-5">
                <CustomInput name="email" type="text" placeholder="Почта" />
                <CustomInput name="password" type="password" placeholder="Пароль" />
                <CustomInput name="repeatPassword" type="password" placeholder="Повторите пароль" />

                <div className="form-field pt-4">
                  <div className="form-control justify-between">
                    <Button type="submit" isLoading={mutation.isPending}>
                      Зарегистрироваться
                    </Button>
                  </div>
                </div>

                <div className="form-field">
                  <div className="form-control justify-center">
                    <Link className="link link-primary link-underline-hover text-sm" to="/login">
                      Есть аккаунт? Войти.
                    </Link>
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
