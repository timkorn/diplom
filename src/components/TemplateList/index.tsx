import { useQuery } from '@tanstack/react-query';
import { getTemplateList } from '../../network/templates/templatesApi';
import { useNavigate } from '@tanstack/react-router';

export const NewTemplatePage = () => {
  const navigate = useNavigate();

  const navigateToSample = (id: string) => {
    navigate({ to: `/template/$templateId`, params: { templateId: id } });
  };

  const { data, isPending, isError } = useQuery({
    queryKey: ['contract'],
    queryFn: getTemplateList,
  });
  if (isError) {
    return null;
  }

  return (
    <div className="pl-[200px] pt-[30px]">
      <div className="max-w-[900px]">
        <div className="text-xl font-semibold max-md:max-w-full">Выбор шаблона договора</div>
        <div className="mt-[60px] flex flex-wrap gap-[20px]">
          {isPending
            ? Array(5)
                .fill(0)
                .map(() => <div className="skeleton h-[112px] w-[160px] rounded-[10px]" />)
            : data.data.templates.map((templ) => (
                <div
                  onClick={() => navigateToSample(templ.id.toString())}
                  className="text-neutral-800 relative flex h-[120px]
                   w-[160px] cursor-pointer items-start items-end overflow-hidden
                    rounded-xl bg-white px-2.5 pb-4 text-xs font-medium shadow-lg"
                >
                  <div className=" max-h-[60px] overflow-hidden">
                    <div className="absolute left-[16px] top-[16px]">
                      <svg
                        width="16"
                        height="16"
                        viewBox="0 0 16 16"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          x="1"
                          y="1"
                          width="4.95654"
                          height="4.95654"
                          rx="1.5"
                          stroke="#687590"
                          stroke-width="2"
                        />
                        <rect
                          x="1"
                          y="10.0439"
                          width="4.95654"
                          height="4.95654"
                          rx="1.5"
                          stroke="#687590"
                          stroke-width="2"
                        />
                        <rect
                          x="10.0435"
                          y="1"
                          width="4.95654"
                          height="4.95654"
                          rx="1.5"
                          stroke="#687590"
                          stroke-width="2"
                        />
                        <rect
                          x="9.73914"
                          y="9.73926"
                          width="2.08696"
                          height="2.08696"
                          rx="0.5"
                          fill="#687590"
                        />
                        <rect
                          x="11.826"
                          y="11.8257"
                          width="2.08696"
                          height="2.08696"
                          rx="0.5"
                          fill="#687590"
                        />
                        <rect
                          x="13.9128"
                          y="9.73877"
                          width="2.08696"
                          height="2.08696"
                          rx="0.5"
                          fill="#687590"
                        />
                        <rect
                          x="9.73889"
                          y="13.9126"
                          width="2.08696"
                          height="2.08696"
                          rx="0.5"
                          fill="#687590"
                        />
                        <rect
                          x="13.9128"
                          y="13.9126"
                          width="2.08696"
                          height="2.08696"
                          rx="0.5"
                          fill="#687590"
                        />
                      </svg>
                    </div>
                    <>{`${templ.title}`}</>
                  </div>
                </div>
              ))}
        </div>
      </div>
    </div>
  );
};
