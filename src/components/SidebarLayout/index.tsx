import { useQuery } from '@tanstack/react-query';
import { Link, Outlet } from '@tanstack/react-router';
import { getProfile } from '../../network/profile/profileApi';

type SidebarLinkProps = {
  name: string;
  route: '/template/newTemplate' | '/docksList/forms' | '/profile';
};

const SidebarLink = ({ name, route }: SidebarLinkProps) => {
  return (
    <Link to={route} className="flex items-center gap-[8px] py-[8px]">
      {route === '/template/newTemplate' && (
        <svg
          width="19"
          height="18"
          viewBox="0 0 19 18"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M9.05263 14.3333H2.78947C2.31488 14.3333 1.85972 14.146 1.52412 13.8126C1.18853 13.4792 1 13.0271 1 12.5556V2.77778C1 2.30628 1.18853 1.8541 1.52412 1.5207C1.85972 1.1873 2.31488 1 2.78947 1H6.36842L9.05263 3.66667H15.3158C15.7904 3.66667 16.2455 3.85397 16.5811 4.18737C16.9167 4.52076 17.1053 4.97295 17.1053 5.44444V8.55556M12.6316 14.3333H18M15.3158 11.6667V17"
            stroke="#3185FC"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      )}
      {route === '/profile' && (
        <svg
          width="18"
          height="18"
          viewBox="0 0 18 18"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1 5V3C1 2.46957 1.21071 1.96086 1.58579 1.58579C1.96086 1.21071 2.46957 1 3 1H5M1 13V15C1 15.5304 1.21071 16.0391 1.58579 16.4142C1.96086 16.7893 2.46957 17 3 17H5M13 1H15C15.5304 1 16.0391 1.21071 16.4142 1.58579C16.7893 1.96086 17 2.46957 17 3V5M13 17H15C15.5304 17 16.0391 16.7893 16.4142 16.4142C16.7893 16.0391 17 15.5304 17 15V13M5 13C5 12.4696 5.21071 11.9609 5.58579 11.5858C5.96086 11.2107 6.46957 11 7 11H11C11.5304 11 12.0391 11.2107 12.4142 11.5858C12.7893 11.9609 13 12.4696 13 13M7 6C7 6.53043 7.21071 7.03914 7.58579 7.41421C7.96086 7.78929 8.46957 8 9 8C9.53043 8 10.0391 7.78929 10.4142 7.41421C10.7893 7.03914 11 6.53043 11 6C11 5.46957 10.7893 4.96086 10.4142 4.58579C10.0391 4.21071 9.53043 4 9 4C8.46957 4 7.96086 4.21071 7.58579 4.58579C7.21071 4.96086 7 5.46957 7 6Z"
            stroke="#3185FC"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      )}

      {route === '/docksList/forms' && (
        <svg
          width="17"
          height="14"
          viewBox="0 0 17 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M5 1H16M5 7H16M5 13H16M1 1V1.01M1 7V7.01M1 13V13.01"
            stroke="#3185FC"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      )}
      <div>{name}</div>
    </Link>
  );
};

export const SidebarLayout = () => {
  const { data } = useQuery({
    queryKey: ['profile'],
    queryFn: getProfile,
  });

  return (
    <div className="w-fill flex h-full">
      <div className="text-neutral-800 flex h-screen w-[230px] flex-col items-start bg-white py-6 pl-4 pr-11 text-[14px] shadow-lg">
        {!data?.data ? (
          <div className="h-[48px]">
            <div className="skeleton h-[20px] w-[120px] rounded-lg" />
            <div className="skeleton mt-3 h-[16px] w-[100px] rounded-lg" />
          </div>
        ) : (
          <div>
            <div className="text-sm font-medium text-black">{data.data.title}</div>
            <div className="mt-3 text-xs text-slate-500">ИНН: {data.data.inn}</div>
          </div>
        )}
        <div className="mt-9 grow">
          <SidebarLink name="Новый договор" route="/template/newTemplate" />
          <SidebarLink name="Список договоров" route="/docksList/forms" />
          <SidebarLink name="Профиль" route="/profile" />
        </div>
      </div>

      <div className="h-screen max-h-screen w-full">
        <Outlet />
      </div>
    </div>
  );
};
