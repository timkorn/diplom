import type { TLoginResponse } from 'src/network/auth/types';
import UniversalCookie from 'universal-cookie';

class Cookies extends UniversalCookie {
  setTokens = (tokens: TLoginResponse) => {
    this.set('token', tokens.token, { path: '/' });
    this.set('refresh', tokens.refresh, { path: '/' });
  };

  setAccessToken = (token: string) => {
    this.set('token', token, { path: '/' });
  };

  deleteTokens = () => {
    this.remove('token', { path: '/' });
    this.remove('refresh', { path: '/' });
  };

  getAccessToken = (): string => this.get('token');
  getRefreshToken = (): string => this.get('refresh');

  checkTokens = (): Boolean => !!this.get('token') && !!this.get('refresh');
}

export const clientCookies = new Cookies();
