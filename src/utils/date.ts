export const fromSecondsToDMY = (seconds: number): string => {
  let date = new Date(seconds * 1000);
  let day = date.getDate();
  let month = date.getMonth();
  let year = date.getFullYear();

  return day + '.' + month + '.' + year;
};
