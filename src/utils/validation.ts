import { z } from 'zod';

const passwordValidation = z
  .string()
  .trim()
  .min(1, 'Требуется пароль')
  .min(8, 'Минимум 8 символов')
  .max(20, 'Максимум 20 символов')
  .regex(new RegExp('^[a-zA-Z0-9]+$'), 'Только латинские буквы и цифры')
  .regex(new RegExp('[0-9]'), 'Минимум одня цифра')
  .regex(new RegExp('[A-Z]'), 'Минимум одна заглавная латинская буква');

const emailValidation = z
  .string()
  .trim()
  .min(1, 'Требуется почта')
  .email()
  .max(250, 'Почта слишком длинная');

const checkFieldToExist = z
  .string()
  .trim()
  .min(1, 'Заполните поле')
  .max(250, 'Превышено максимальное количество символов');

export const newPasswordSchema = z
  .object({
    password: passwordValidation,
    repeatPassword: passwordValidation,
  })
  .superRefine(({ repeatPassword, password }, ctx) => {
    if (repeatPassword !== password) {
      ctx.addIssue({
        code: 'custom',
        message: 'Пароли не совпадают',
        path: ['repeatPassword'],
      });
    }
  });

export const loginSchema = z.object({
  email: checkFieldToExist,
  password: checkFieldToExist,
});

export const registrSchema = z
  .object({
    email: emailValidation,
    password: passwordValidation,
    repeatPassword: passwordValidation,
  })
  .superRefine(({ repeatPassword, password }, ctx) => {
    if (repeatPassword !== password) {
      ctx.addIssue({
        code: 'custom',
        message: 'Пароли не совпадают',
        path: ['repeatPassword'],
      });
    }
  })
  .superRefine(({ email, password, repeatPassword }, ctx) => {
    if (repeatPassword === email) {
      ctx.addIssue({
        code: 'custom',
        message: 'Недопустимый пароль',
        path: ['repeatPassword'],
      });
    }
    if (password === email) {
      ctx.addIssue({
        code: 'custom',
        message: 'Недопустимый пароль',
        path: ['password'],
      });
    }
  });

export const restoreEmailSchema = z.object({
  email: emailValidation,
});

export const registerOrgSchema = z.object({
  address_legal: checkFieldToExist,
  inn: checkFieldToExist,
  title: checkFieldToExist,
});

export const sendCodeSchema = z.object({
  code: checkFieldToExist,
});

export const checkAllFieldsToExist = (obj: { [keys: string]: any }) => {
  const keys = Object.keys(obj);
  const validationObj = Object.fromEntries(keys.map((key) => [key, checkFieldToExist]));

  return z.object(validationObj);
};
