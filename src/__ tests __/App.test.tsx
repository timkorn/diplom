import { render } from '@testing-library/react';
import { Login } from '../components/Login';
import { Register } from '../components/Register';
import { SidebarLayout } from '../components/SidebarLayout';
import { Profile } from '../components/Profile';

test('Renders login page', () => {
  render(<Login />);
  expect(true).toBeTruthy();
});

test('Renders register page', () => {
  render(<Register />);
  expect(true).toBeTruthy();
});

test('Renders layout page', () => {
  render(<SidebarLayout />);
  expect(true).toBeTruthy();
});

test('Renders profile page', () => {
  render(<Profile />);
  expect(true).toBeTruthy();
});
